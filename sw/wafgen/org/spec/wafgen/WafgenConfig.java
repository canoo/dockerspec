//package wafgen.application;

package org.spec.wafgen;

import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;
//import wafgen.support.*;
//import wafgen.application.*;

/**
 * <b>WafgenConfig</b> is a <i>HashTable</i> of name-value pairs. All
 * all the possible names need to be initialized with their default
 * values. WafgenConfig is read from the rc file and is parsed into
 * variables. Use <code>getValue(key)</code> to get the value for a given
 * name. Use <code>setValue(key, value)</code> to set the value. Name should
 * be unique and is the key for hashing.
 * <p>
 * <b>WafgenConfig</b> implements <i>Serializable</i> and is passed to load
 * generators through RMI. Each load generator receives a copy of the
 * configuration from the prime client during the set up phase.
 *
 * @see java.io.Serializable
 * @see java.util.HashTable
 *
 */

public class WafgenConfig implements Serializable {

    private Hashtable config;	// configuration pairs

    //private static String filesep = System.getProperty("file.separator");
    private static String filesep = "/";

    private static int debugVal = 0;

    /*
    * WafgenConfig constructor
    */
    WafgenConfig() {
	config = new Hashtable();
    }

    public final Hashtable getConfig() {
	return config;
    }

    /*************************************************************
     * Methods dealing with values
     *************************************************************/

    /**
    * Take a configuration key and decide if it describes part of a vector
    */
    private static Vector mungeConfigEntry(String orig) {
        Vector retval = new Vector(4,1);
        StringTokenizer toke = new StringTokenizer(orig, "[]");
        while(toke.hasMoreTokens()) {
            retval.addElement((String)toke.nextToken().toUpperCase());
        }
        return retval;
    }

    public void setValue(String key, String value) {
	String oldValue = getValue(key);
	if (oldValue!=null && !value.equals(oldValue)) { 
            error ("Changed value of configuration variable " + key);
	}
	config.put(key.toUpperCase(), value);
    }

    public void changeValue(String key, String value) {
	config.put(key.toUpperCase(), value);
    }
    
    private int nextError = 0;
    public void error(String message) {
	if (nextError == 0) {
	    System.err.println("*******************************************");
	    changeValue("INVALID_REASON", message);
	}
	System.err.println("Warning: " + message);
	if (nextError == 0) {
	    System.err.println("Warning: ... therefore this is not a valid SPEC run! ********************");
	}
	changeValue("ERRORS["+nextError+"]", message);
	nextError++;
    }
    
    public String getValue(String entry) {
	Vector key = mungeConfigEntry(entry.toUpperCase());
        // Key should always have at least 1 value: the key
        if (key.size() < 1)
            return null;
        /* Okay, there's a key... if there are no more elements in the
         * vector, just get it and go.
         */
        if (key.size() == 1)
	    return (String)config.get(entry.toUpperCase());

        /* The key is a reference to a vector element somewhere.  */
        String keyname = (String)key.firstElement();
        key.removeElementAt(0);

        /* First get a reference to the vector itself.  */
        Object tmp = getObject(keyname.toUpperCase());
        Vector v;
        if ((tmp == null) ||
            (!tmp.getClass().getName().equals("java.util.Vector"))) {
	    return null;
        } else {
            v = (Vector)tmp;
        }

        /* Now that we've got a refence to the root vector (attached to the
         * configuration table), iterate over the elements in key, which
         * should be indices into the vectors.
         */
        Enumeration indices = key.elements();
	int idx = 0;
	String idxString = new String("");
        while (indices.hasMoreElements()) {
	    try {
		idxString = (String)indices.nextElement();
		idx = Integer.parseInt(idxString, 10);
	    } catch (NumberFormatException e) {
		System.err.println("Error: \""+idxString+"\" (used as configuration element index) is not an integer");
		e.printStackTrace();
		System.exit(-1);
	    }
	    if (debugVal == 5) println("Looking at v["+idx+"]");
	    // Check to see if there's a value there
	    try {
		tmp = v.elementAt(idx);
	    } catch (ArrayIndexOutOfBoundsException foo) {
		tmp = null;
	    }
	    if (tmp != null) {
		if (tmp.getClass().getName().equals("java.util.Vector")) {
		    // It's a vector.  Set v to it and loop
		    v = (Vector)tmp;
		} else if (tmp.getClass().getName().equals("java.lang.String")) {
		    // It's our data
		    return (String)tmp;
		}
	    } else {
		return null;
            }
        }
	return null; // never reached
    }
    
    public boolean getTraceValue(String entry) {
	String str=getValue(entry.toUpperCase());
	if (str==null || str.length()==0 || str.equals("0")) return false;
	return true;
    }
    
    
    public double getDoubleValue(String entry) {
	entry = entry.toUpperCase();
	String str=getValue(entry);
	if (str == null) {
	  System.err.println("Error: missing configuration variable " + entry);
	  System.exit(-1);
	}
	return Double.valueOf(str).doubleValue();
    }
    
    public void setObject(String entry, Object value) {
	config.put(entry.toUpperCase(), value);
    }
    
    public Object getObject(String entry) {
	return (Object)config.get(entry.toUpperCase());
    }
    
    private static void setConfigValue(WafgenConfig config, Vector key,
                                       String value) {
        // Key should always have at least 1 value: the key
        if (key.size() < 1)
            return;
        /* Okay, there's a key... if there are no more elements in the
         * vector, just set it and go.
         */
        if (key.size() == 1) {
            config.setValue((String)key.firstElement(), value);
            return;
        }
        /* The key is a reference to a vector element somewhere.
         */
        String keyname = (String)key.firstElement();
        key.removeElementAt(0);
        /* First get a reference to the vector itself.  If it's not there,
         * make a new one
         */
        Object tmp = config.getObject(keyname);
        Vector v;
        if ((tmp == null) ||
            (!tmp.getClass().getName().equals("java.util.Vector"))) {
            // Empty or non-vector, either way we put a vector there.
            v = new Vector(1,1);
            v.setSize(1);
            config.setObject(keyname, v);
        } else {
            v = (Vector)tmp;
        }

        /* Now that we've got a refence to the root vector (attached to the
         * configuration table), iterate over the elements in key, which
         * should be indices into the vectors.
         */
        Enumeration indices = key.elements();
        while (indices.hasMoreElements()) {
            int idx = Integer.parseInt((String)indices.nextElement(), 10);
	    if (debugVal == 5) println("Working on index " + idx);
            if (v.capacity() <= idx) {
                v.ensureCapacity(idx + 1);
                v.setSize(idx + 1);
		if (debugVal == 5) println("v's capacity is now " + v.capacity());
                // No element, so just set it if necessary
                v = setConfVecElement(indices, value, v, idx);
                if (v == null)
                    return;
            } else {
		if (debugVal == 5) println("Looking at v["+idx+"]");
                // Check to see if there's a value there
                try {
                    tmp = v.elementAt(idx);
                } catch (ArrayIndexOutOfBoundsException foo) {
                    tmp = null;
                }
                if (tmp != null &&
                    tmp.getClass().getName().equals("java.util.Vector")) {
                    // It's a vector.  Set v to it and loop
                    v = (Vector)tmp;
                } else {
		    if (debugVal == 5) println("Using preexisting vector");
                    v = setConfVecElement(indices, value, v, idx);
                    if (v == null)
                        return;
                }
            }
        }
    }

    private static Vector setConfVecElement(Enumeration indices, String value,
					    Vector current, int idx) {
        if (indices.hasMoreElements()) {
            // The thing should be replaced with a vector
            Vector newvec = new Vector(1,1);
            newvec.ensureCapacity(1);
            newvec.setSize(1);
            current.ensureCapacity(idx + 1);
            if (current.size() <= idx)
                current.setSize(idx + 1);
            current.setElementAt(newvec, idx);
            return newvec; // Set up for the next pass
        } else {
            // The new thing is the value
            current.setElementAt(value, idx);
            return null;
        }
    }
    

    public int getConfigKeyCount(String key) {
	Object tmp = getObject(key);
	if (tmp == null)
	    return 0;
	if (tmp.getClass().getName().equals("java.util.Vector")) {
	    Vector v = (Vector)tmp;
	    // Fill the lines array with the contents of any vectors
	    return v.size();
	} else {
	    return 1;
	}
    }

    public String getConfigString(String key, boolean quote) {
	Object tmp = getObject(key);
//	int i = 0;
	if (tmp == null)
	    return "";
	if (tmp.getClass().getName().equals("java.util.Vector")) {
	    Vector v = (Vector)tmp;
	    // Fill the lines array with the contents of any vectors
	    return dumpVector(key, v, quote, 0);
	} else {
	    if (quote)
		return (key + " = \"" + (String)tmp + "\"\n");
	    else
		return (key + " = " + (String)tmp + "\n");
	}
    }

    /*
     * Read and parse a configuration rc file
     */
    private void readFile(String file) throws IOException {
	final WafgenConfig cfg = this;

	/*
	 * First read in the whole file so that it can be compressed and
	 * base64 encoded for posterity.
	 */

        FileReader reader;
        try {
	    reader = new FileReader(file);
        } catch (Exception e) {
	    System.err.println("Error: File " + file + " does not exist.");
	    e.printStackTrace();
	    System.exit(-1);
	    return; 
        }

        StreamTokenizer tokenizer = new StreamTokenizer(reader);
        tokenizer.resetSyntax();
	tokenizer.wordChars(' '+1, 255);
        tokenizer.commentChar('#');
        tokenizer.ordinaryChar(';');
        tokenizer.ordinaryChar(',');
        tokenizer.ordinaryChar('\r');
        tokenizer.ordinaryChar('\t');
        tokenizer.ordinaryChar('=');
        tokenizer.quoteChar('"');
        tokenizer.slashStarComments(true);
        tokenizer.slashSlashComments(true);
        tokenizer.eolIsSignificant(true);

        int t;
        int state = 0;
	/* The "entry" vector will always contain at least 1 element: the
	 * key name.  Other elements, if any, will be indices into a vector
	 * of values.
	 */
	Vector entry = new Vector(4,1);
        while ((t=tokenizer.nextToken()) != StreamTokenizer.TT_EOF) {
	    if ((tokenizer.ttype == '"') && (state == 1)) {
		if (debugVal == 5) println("Entry read: "+entry+" "+tokenizer.sval);
		setConfigValue(cfg, entry, tokenizer.sval);
		state = 0;
	    }
	    else switch (t) {
	    case StreamTokenizer.TT_WORD:
		if ((state == 0) && (tokenizer.sval != null)) {
		    entry = mungeConfigEntry(tokenizer.sval);
		    state = 1;
		}
		else {
		    if (debugVal == 5) println("Entry read: "+entry+" "+tokenizer.sval);
		    setConfigValue(cfg, entry, tokenizer.sval);
		    state = 0;
		}
		break;
	    case StreamTokenizer.TT_EOL:
		state = 0;
		break;
	    default:
		break;
	    }
        }
	reader.close();
    }
    

    public static boolean aboutEqual(double x, double y) {
	double diff = Math.abs(x - y);
	double scale = Math.abs(x) + Math.abs(y);
	return (diff/scale) < 0.0000001;
    }

    
    
    /*
    * Create a new WafgenConfig from the three RC files
    */
    public static WafgenConfig readFrom(String file1) throws IOException {
        WafgenConfig cfg = new WafgenConfig();
	if (file1 != null) cfg.readFile(file1);
	debugVal = cfg.getDEBUG();
	return cfg;
    }

    private String dumpVector(String prepend, Vector vec, boolean quote,
			      int depth) {
	int i = 0;
	String retval = new String();
	Object tmp;
	// Arbitrarily limit the depth, in case we encounter a circular
	// reference.  Dumb but effective.
	if (depth > 10)
	    return "";
	for(i = 0; i < vec.size(); i++) {
	    tmp = vec.elementAt(i);
	    if (tmp == null) {
		if (quote)
		    retval += prepend + "[" + i + "] = \"\"\n";
		else
		    retval += prepend + "[" + i + "] =\n";
	    } else if (tmp.getClass().getName().equals("java.lang.String")) {
		if (quote)
		  retval += prepend + "[" + i + "] = \"" + (String)tmp + "\"\n";
		else
		  retval += prepend + "[" + i + "] = " + (String)tmp + "\n";
	    } else if (tmp.getClass().getName().equals("java.util.Vector")) {
		retval += dumpVector(prepend + "[" + i + "]", (Vector)tmp,
				     quote, ++depth);
	    }
	}
	return retval;
    }


    static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss:SSS");
    
    /**
     * Returns a formated timestamp.
     */
    private static synchronized String formatCurrentTime() {
	return dateFormat.format(new Date());
    }
 
    /**
     * Print a line preceded by a timestamp.
     */
    public static void println(String s) {
	System .out .println(formatCurrentTime() + " " + s);
    }


    String getDocRoot() {
	String docroot = getValue("DOCROOT");
	//if (docroot == null) return "";
	return docroot;
    }

    String getChangeDirectory() {
	String docroot = getValue("DOCROOT");
	return docroot;
    }

    String getBaseDirName() {
	String basedir = getValue("BASEDIRNAME");
	return basedir;
    }

    String getBaseSubDirName() {
	String basedir = getValue("BASESUBDIRNAME");
	return basedir;
    }

    String getInclString() {
	return getValue("INCLSTRING");
    }

    int getFirstDirectory() {
	int retval=0;
        try {
            retval=Integer.parseInt(getValue("FIRSTDIRECTORY"));
        } catch (Exception e) { retval=0;}
	return retval;
    }

    int getFirstSubDirectory() {
	int retval0=0;
	int retval1=0;
        try {
            retval0=Integer.parseInt(getValue("FIRSTSUBDIRECTORY"));
        } catch (Exception e) { retval0=0;}
	if (retval0 == 0) {
            try {
                retval1=Integer.parseInt(getValue("FIRST_SUBDIR_NUM"));
            } catch (Exception e) { retval1=0;}
	    retval0 = retval1;
 	}
	return retval0;
    }

    int getLastDirectory() {
	int retval=0;
        try {
            retval=Integer.parseInt(getValue("LASTDIRECTORY"));
        } catch (Exception e) { retval=0;}
	return retval;
    }


    int getBaseNumDirs() {
	int retval=0;
        try {
            retval=Integer.parseInt(getValue("BASENUMDIRS"));
        } catch (Exception e) { retval=0;}
	return retval;
    }

    int getDEBUG() {
	int retval=0;
        try {
            retval=Integer.parseInt(getValue("DEBUG"));
        } catch (Exception e) { retval=0;}
	return retval;
    }


    double getDirScaling() {
	double retval0=0.0;
	double retval1=0.0;
        try {
            retval0 = Double.parseDouble(getValue("DIRSCALING"));
        } catch (Exception e) { retval0=0.0;}
	if ( retval0 == 0.0) {
            try {
              retval1 = Double.parseDouble(getValue("CLIENT_SESSION_USER_IDS"));
            } catch (Exception e) { retval1=0.0;}
	retval0 = retval1;
	}
	return retval0;
    }

    double getSubDirScaling() {
	double retval0=0.0;
	double retval1=0.0;
	double retval2=0.0;
	double retval3=0.0;
        try {
            retval0 = Double.parseDouble(getValue("SUBDIRSCALING"));
        } catch (Exception e) { retval0=0.0;}
	if (retval0 == 0.0) {
            try {
                retval1 = Double.parseDouble(getValue("CHECK_IMAGE_SUBDIRS"));
            } catch (Exception e) { retval1=0.0;}
	    retval0 = retval1;
	}
	if (retval0 == 0.0) {
            try {
                retval2 = Double.parseDouble(getValue("PRODUCT_IMAGE_SUBDIRS"));
            } catch (Exception e) { retval2=0.0;}
	    retval0 = retval2;
        }
	if (retval0 == 0.0) {
            try {
                retval3 = Double.parseDouble(getValue("DOWNLOAD_SUBDIRS"));
            } catch (Exception e) { retval3=0.0;}
	    retval0 = retval3;
	}
	return retval0;
    }


    int getNumberOfConnections() {
	int retval0=0;
	int retval1=0;
        try {
	    retval0 = Integer.parseInt(getValue("NUMBEROFCONNECTIONS"));
        } catch (Exception e) { retval0=0;}
	if (retval0 == 0) {
            try {
	        retval1 = Integer.parseInt(getValue("SIMULTANEOUS_SESSIONS"));
            } catch (Exception e) { retval1=0;}
	    retval0 = retval1;
        }
	return retval0;
    }


    int getBigBufferSz() {
	int retval=0;
        try {
	    retval = Integer.parseInt(getValue("BIGBUFFERSZ"));
        } catch (Exception e) { retval=0;}
	return retval;
    }


    int getMarkerFreq() {
	int retval0=0;
	int retval1=0;
        try {
	    retval0 = Integer.parseInt(getValue("MARKERFREQUENCY"));
        } catch (Exception e) { retval0=0;}
	if (retval0 == 0) {
            try {
	        retval1 = Integer.parseInt(getValue("MARKER_FREQ"));
            } catch (Exception e) { retval1=0;}
	    retval0 = retval1;
        }
	if (retval0 == 0) retval0 = 4096;
	return retval0;
    }


    int getLineSize() {
	int linesz = 64;
	int retval=0;
        try {
	    retval = Integer.parseInt(getValue("LINESIZE"));
        } catch (Exception e) { retval=0;}
        if (retval <= 0) return linesz;
	return retval;
    }



    String[] getFileDefn(int fileidx) {
        String[] filedefn = { };
	if ( getConfigKeyCount("FILE") >= fileidx+1 ) {
           try {
	       String fileentry = getValue("FILE[" + fileidx + "]");
	   StringTokenizer tokens = new StringTokenizer(fileentry, ":");
	   if (debugVal == 5) System.err.println ("fileentry: " + fileentry);
           int tcount = tokens.countTokens();
           filedefn = new String[tcount];
	   if (debugVal == 5) System.err.println ("tcount = " + tcount);
	   filedefn = new String[tcount];
	   for(int i = 0; i < tcount; i++) {
            filedefn[i] = (String)tokens.nextToken();
	    if (debugVal == 5) System.err.println("filedefn["+ i +"] = "+filedefn[i]);
           }
           } catch (Exception e) {
               System.err.println("Error: Missing Fileidx: " + fileidx);
               System.exit(-1);
               return null;
           }
	   
	}
	return filedefn;
    }

    String getFileName(int fileidx) {
	String[] filedefn = new String[3] ;
	if ( getConfigKeyCount("FILE") >= fileidx+1 ) {
	    filedefn = getFileDefn(fileidx);
            return (String) filedefn[0];
	    //String fileentry = getValue("FILE[" + fileidx + "]");
            //StringTokenizer tokens = new StringTokenizer(fileentry, "\n:");
	    //if (tokens.countTokens() >= 1) {
               //return (String)tokens.nextToken();
            //}
        }
	return null;
    }

    String getFileLen(int fileidx) {
        String[] filedefn = new String[3] ;
	if ( getConfigKeyCount("FILE") >= fileidx+1 ) {
	    filedefn = getFileDefn(fileidx);
            return (String) filedefn[1];
        }
	return null;
    }

    String getFileDir(int fileidx) {
        String[] filedefn = new String[3] ;
	if ( getConfigKeyCount("FILE") >= fileidx+1 ) {
	    filedefn = getFileDefn(fileidx);
            return (String) filedefn[2];
        }
	return null;
    }

    String getFileName(String[] filedefn) {
	String filename = filedefn[0];
	return filename;
    }

    int getFileLen(String[] filedefn) {
	int filelen = Integer.parseInt(filedefn[1]);
//return Double.valueOf(str).doubleValue();
	return filelen;
    }

    String getFileDir(String[] filedefn) {
	String dirname = filedefn[2];
	return dirname;
    }

    int getClassLen(int iclass) {
	int classlen=0;
        try {
            classlen=Integer.parseInt(getValue("CLASS_LEN["+iclass+"]"));
        } catch (Exception e) { classlen=0;}
	return classlen;
    }


    int getClassMinCount(int iclass) {
	int classmincount=0;
        try {
         classmincount=Integer.parseInt(getValue("CLASS_MINCOUNT["+iclass+"]"));
        } catch (Exception e) { classmincount=0;}
	return classmincount;
    }


    int getClassMaxCount(int iclass) {
	int classmaxcount=0;
        try {
         classmaxcount=Integer.parseInt(getValue("CLASS_MAXCOUNT["+iclass+"]"));
        } catch (Exception e) { classmaxcount=0;}
	return classmaxcount;
    }


    int getClassRel(int iclass) {
	int classrel=0;
        try {
            classrel=Integer.parseInt(getValue("CLASS_REL["+iclass+"]"));
        } catch (Exception e) { classrel=0;}
	return classrel;
    }

    int getClassIncr(int iclass) {
	int classincr=0;
        try {
            classincr=Integer.parseInt(getValue("CLASS_INCR["+iclass+"]"));
        } catch (Exception e) { classincr=0;}
	return classincr;
    }


    int getClassCount(int iclass) {
	int classcount=0;
        try {
            classcount=Integer.parseInt(getValue("CLASS_COUNT["+iclass+"]"));
        } catch (Exception e) { classcount=0;}
	return classcount;
    }


    int getClassDiv(int iclass) {
	int classdiv=0;
        try {
            classdiv=Integer.parseInt(getValue("CLASS_DIV["+iclass+"]"));
        } catch (Exception e) { classdiv=0;}
	return classdiv;
    }

    int getClassInclStringLine(int iclass) {
	int classisl=0;
        try {
            classisl=Integer.parseInt(getValue("CLASS_INCLLINE["+iclass+"]"));
        } catch (Exception e) { classisl=0;}
	return classisl;
    }

    int getClassInclCounts(int iclass) {
	int classictr=0;
        try {
            classictr=Integer.parseInt(getValue("CLASS_INCLCTR["+iclass+"]"));
        } catch (Exception e) { classictr=0;}
	return classictr;
    }

    int getFileInclStringLine(int ifile) {
	int isl=0;
        try {
            isl=Integer.parseInt(getValue("FILE_INCLLINE["+ifile+"]"));
        } catch (Exception e) { isl=0;}
	return isl;
    }

    int getFileInclCounts(int ifile) {
	int ictr=0;
        try {
            ictr=Integer.parseInt(getValue("FILE_INCLCTR["+ifile+"]"));
        } catch (Exception e) { ictr=0;}
	return ictr;
    }



}
