/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

/*

    SPECweb2005 Backend Simulator (BESIM)  Support Site Command Module 

*/

#include "besim_supportsite.h"
#include "randomtext.h"
#include <math.h>

#define I_MIN(a, b) (a < b ? a : b)
#define I_MAX(a, b) (a > b ? a : b)

extern int randomtext (char* textbuf, int textlength, int texttype);


void DoSupportCommand(char* Arg[], int ArgCnt, char* MsgTxt){
  int Command;
  int Status;
  int AdjArgCnt;
  int fd;
  int s;
  char *ptr;
  struct stat FStats;

  /* Get Command Code and Adjusted Argument Count */

  Command = atoi(Arg[1]); 
  AdjArgCnt = ArgCnt - 1;
  ptr = MsgTxt;

  /* If ptr to globals have not been initialized by Reset command, then try
  /* to access them from the besim globals file via mmap/read.  This helps
  /* ensure that if the besim deamon dies and restarts or there are multiples
  /* that all the initialization values are available
  */
  if ((Command != 0) && ((gSConstants == NULL) ||
      ((stat(BESIM_SUPPORT_GLOBALS_PATH, &FStats) != -1) &&
       (FStats.st_mtime > gSConstants->lastResetTime)))) {
#ifndef WIN32
      fd = open( BESIM_SUPPORT_GLOBALS_PATH, O_RDWR, 0666 );
#else
      fd = _open( BESIM_SUPPORT_GLOBALS_PATH, O_RDWR, 0666 );
#endif
      if ( fd >= 0 ) {
        errno = 0;
#ifdef USE_MMAP
            gSConstants = mmap(NULL, sizeof(workload_globals_t),
              (PROT_WRITE|PROT_READ), (MAP_FILE|MAP_SHARED), fd, (off_t) 0 );
            if (gSConstants == MAP_FAILED) {
                ptr += sprintf(ptr, ERR1_STATUS);
                ptr += sprintf(ptr, "mmap: %d: %s\n", errno, strerror(errno));
                gSConstants = NULL;
#ifndef WIN32
                close(fd);
#else
                _close(fd);
#endif
                return;
            }
            if ((gSConstants->lastResetTime == 0) ||
                 (FStats.st_mtime > gSConstants->lastResetTime))
                        gSConstants->lastResetTime = FStats.st_mtime;
#else
	    gSConstants = &Constants;
	    s = read(fd, (void *) gSConstants, sizeof(workload_globals_t));
	    if (s != sizeof(workload_globals_t)) {
    	        ptr += sprintf(ptr, ERR1_STATUS);
                ptr += sprintf(ptr, "read: %d: %s\n", errno, strerror(errno));
                gSConstants = NULL;
#ifndef WIN32
                close(fd);
#else
                _close(fd);
#endif
	        return;
            }
            gSConstants->lastResetTime = FStats.st_mtime;
#endif

      } else {
     	    ptr += sprintf(ptr, ERR1_STATUS);
     	    strncpy(ptr, ERROR_BAD_GLOBALS, sizeof(ERROR_BAD_GLOBALS));
            return;
      }
#ifndef WIN32
      close(fd);
#else
      _close(fd);
#endif
  }

  /* Validate: Command Code, Argument Count and 1st Argument for Command */
  /* Invoke Routine to process the specified command                     */
  
  if (Command >= 0 && Command <  (sizeof(support_cmdFp)/sizeof(support_cmdFp[0]))) {

   if (gSConstants != NULL && gSConstants->gflag == 9 && gSConstants->Load > 0) 
	srand((unsigned int) gSConstants->Load);
   
   if (AdjArgCnt >= support_cmdArgCnt[Command]) 
     Status = Support_Valid1stArg(Command, Arg[2], MsgTxt)? 
        (*support_cmdFp[Command])(&Arg[2], AdjArgCnt, MsgTxt): -1;
   else {
     ptr += sprintf(ptr, ERR1_STATUS);
     strncpy(ptr, ERROR_BAD_ARGS, sizeof(ERROR_BAD_ARGS));
   }
  }
  else {
    ptr += sprintf(ptr, ERR1_STATUS);
    strncpy(ptr, ERROR_UNDEF_CMD, sizeof(ERROR_UNDEF_CMD));
  }
  

}




int Support_Valid1stArg(int cmd, char* arg1, char* msg) {
/*    int firstarg;*/
    char *ptr;

    ptr = msg;

    /* Validate 1st argument for Ecommerce Commands, All commands */
    /* except Reset(0) should have a valid region id              */
  /*  if (cmd == 11) { firstarg=gSConstants->Max_region;  return 1; }
    firstarg = atoi(arg1);
    if (cmd == 0 && firstarg != 0 ) return 1;
    if (VALID(firstarg,gSConstants->Min_region,gSConstants->Max_region)) return 1;
    else {
      ptr += sprintf(ptr, ERR1_STATUS);
      ptr += sprintf(ptr, "firstarg = %d, Min_region = %d, Max_region = %d\n",
                 firstarg,gSConstants->Min_region,gSConstants->Max_region);
      strncpy(ptr, ERROR_BAD_ARG1, sizeof(ERROR_BAD_ARG1));
    }
*/
    return 1;

}


/*
  Support Command: 0 - Reset
  Request Params:  N_TIME&N_Load
  Response:	   S_Status (DONE)
  Script Mapping:  Prime Client Only
  DB Op:		    NA
*/

int Support_Reset (char* Arg[], int ArgCnt, char* MsgTxt){
    
    int len = 0;
    time_t now;
    const struct tm *tmstruct;
    char *ptr;
    int fd;
    struct stat FStats;

    ptr = MsgTxt;

    /* Get YYYYMMDD and return to Prime Client */
    /* May be used for Date fields in response and/or validation */

    (void)setlocale(LC_ALL, "");
    time(&now);
    tmstruct = localtime(&now);
    strftime(Constants.ResetDate, 16, "%Y%m%d", tmstruct);

    /* Save values for use in subsequent commands */
    Constants.Time = atoi(Arg[0]);
    Constants.Load = atoi(Arg[1]);
    Constants.Scaled_Load = (int) (sqrt(atof(Arg[1]))/2.0);
    if (Constants.Scaled_Load == 0) Constants.Scaled_Load = 1;
    Constants.gflag = 9;
    Constants.lastResetTime = 0;

    /* save the globals to file for use by other Besim deamons */
    unlink (BESIM_SUPPORT_GLOBALS_PATH);
#ifndef WIN32
    fd = open( BESIM_SUPPORT_GLOBALS_PATH, O_RDWR|O_CREAT, 0666 );
#else
    fd = _open( BESIM_SUPPORT_GLOBALS_PATH, O_RDWR|O_CREAT, 0666 );
#endif
    if ( fd >= 0 ) {
        write (fd, &Constants, sizeof(workload_globals_t));
        fsync(fd);
        gSConstants = &Constants;
        if (-1 == stat( BESIM_SUPPORT_GLOBALS_PATH, &FStats) ) {
                ptr += sprintf(ptr, ERR1_STATUS);
                strncpy(ptr, ERROR_BAD_GLOBALS, sizeof(ERROR_BAD_GLOBALS));
#ifndef WIN32
                close(fd);
#else
                _close(fd);
#endif
                return 0;
        } else {
                lseek (fd, 0, SEEK_SET);
                gSConstants->lastResetTime = FStats.st_mtime;
                write (fd, &Constants, sizeof(workload_globals_t));
                fsync(fd);
#ifndef WIN32
                sleep (1);
#else
		Sleep (1000);
#endif

        }
#ifndef WIN32
        close(fd);
#else
        _close(fd);
#endif


    } else {
        ptr += sprintf(ptr, ERR1_STATUS);
     	strncpy(ptr, ERROR_BAD_GLOBALS, sizeof(ERROR_BAD_GLOBALS));
	return 0;
    }

    ptr += sprintf(ptr, OK_STATUS);
    sprintf(ptr,
      "DONE ResetDate = %s, Time=%d,Load=%d,SL=%d\n",
      gSConstants->ResetDate, gSConstants->Time, 
      gSConstants->Load, gSConstants->Scaled_Load);

    return 0;
}


int Support_Undef (char* Arg[], int ArgCnt, char* MsgTxt){
char *ptr;

    ptr = MsgTxt;
    ptr +=  sprintf(ptr,ERR1_STATUS);
    strncpy(MsgTxt, ERROR_UNDEF_CMD, sizeof(ERROR_UNDEF_CMD));
    return 0;
}
 
/*
  Support Command: 1 - Category Listing
  Request Params:  (none)
  Response:	   S_Status
		   N_Results:
		   N_ProductCategoryID1&S_ProductCategory1
		   N_ProductCategoryID2&S_ProductCategory2...

  Script Mapping:  Index
  DB Op:	   Select
*/
int CategoryListing ( char *Arg[], int ArgCnt, char *MsgTxt) {
    char *ptr;
    int i;
    int len;
    int num_of_cats;
    int key;
    char Category[LONGNAMELEN+1];

    ptr = MsgTxt;
    
    num_of_cats = 5 + (int) (gSConstants->Scaled_Load/5);
    key = gSConstants->Time%10000;
		 
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_cats; i++) {
      len = 0;
      len += randomtext (Category, LONGNAMELEN-2, LONGNAME);
      ptr += sprintf(ptr,"%04d&%s\n",i+key,  Category);
    }
    
    return 0;
}

/*
  Support Command: 2 - Product Listing
  Request Params:  N_ProductCategoryID
  Response:	   S_Status
		   N_Results:
		   N_ProductID1&S_Product1
		   N_ProductID2&S_Product2...

  Script Mapping:  Catalog
  DB Op:	   Select
*/
int ProductListing ( char *Arg[], int ArgCnt, char *MsgTxt) {
    int ncatid;
    char *ptr;
    int i;
    int len;
    int num_of_cls;
    int key;
    char Product[PRODUCTLEN+1];

    ptr = MsgTxt;
    ncatid = atoi (Arg[0]);
    
    num_of_cls = 10 + (int) (gSConstants->Scaled_Load/3);
    key = gSConstants->Time%1000;
		 
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_cls; i++) {
      len = 0;
      len += randomtext (Product, PRODUCTLEN-2, PRODUCT);
      ptr += sprintf(ptr,"%04d&%s\n", i+key, Product);
    }
    
    return 0;
}

/*
  Support Command: 3 - Search
  Request Params:  S_keyword1+S_keyword2+S_keyword3...
  Response:	   S_Status
		   N_Results:
		   N_ProductID1&S_Product1
		   N_ProductID2&S_Product2...

  Script Mapping:  Search
  DB Op:	   Select
*/
int SearchProducts ( char *Arg[], int ArgCnt, char *MsgTxt) {

    char *ptr;
    char *lptr;
    int i;
    int listcount;
    int len=0;
    int num_of_finds;
    int key;
    char Product[PRODUCTLEN+1];

    ptr = MsgTxt;
    
    for (listcount=1, lptr = Arg[0]; *lptr != '\0'; lptr++) {
		if ('+' == *lptr) listcount++;
    }
    num_of_finds = I_MIN(20, 1+((int)((gSConstants->Scaled_Load * 1.5)/listcount)));
    key = gSConstants->Time%10000;
		 
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_finds; i++) {
      len += randomtext (Product, PRODUCTLEN-2, PRODUCT);
      ptr += sprintf(ptr,"%04d&%s\n", i+key, Product);
      len = 0;
    }

    return 0;
}


/*
  Support Command: 4 - Download Categories
  Request Params:  N_ProductID
  Response:	   S_Status
		   N_Results:
		   S_DownloadCategoryID1
		   S_DownloadCategoryID2...

  Script Mapping:  Product
  DB Op:	   Select
*/
int DownloadCategories ( char *Arg[], int ArgCnt, char *MsgTxt) {

    char *ptr;
    int i;
/*    int len;*/
    int num_of_dcs;

    ptr = MsgTxt;
    
    num_of_dcs = sizeof(DnldCatList)/sizeof(DnldCatList[0]);
		 
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_dcs; i++) {
      ptr += sprintf(ptr,"%s\n", DnldCatList[i]);
    }
    return 0;
    
}
/*
  Support Command: 5 - Languages
  Request Params:  (none)
  Response:	   S_Status
		   N_Results:
		   S_Language1
		   S_Language2...

  Script Mapping:  Product
  DB Op:	   Select
*/
int Languages ( char *Arg[], int ArgCnt, char *MsgTxt) {
    char *ptr;
    int i;
/*    int len;*/
    int num_of_ls;

    ptr = MsgTxt;
    
    num_of_ls = sizeof(LanguageList)/sizeof(LanguageList[0]);
		 
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_ls; i++) {
      ptr += sprintf(ptr,"%s\n", LanguageList[i]);
    }
    
    return 0;
}

/*
  Support Command: 6 - Operating Systems
  Request Params:  N_ProductID
  Response:	   S_Status
		   N_Results:
		   S_OS1
		   S_OS2...

  Script Mapping:  Product
  DB Op:	   Select
*/

OpSys ( char *Arg[], int ArgCnt, char *MsgTxt) {
    char *ptr;
    int i;
/*    int len;*/
    int num_of_ls;
/*    char OSname[LONGNAMELEN+1];*/

    ptr = MsgTxt;
    
    num_of_ls = sizeof(OSList)/sizeof(OSList[0]);
		 
    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_ls; i++) {
      ptr += sprintf(ptr,"%s\n", OSList[i]);
    }
    
    return 0;
}


/*
  Support Command: 7 - Files
  Request Params:  S_ProductID&S_DownloadCategory&S_Language&S_OS
  Response:	   S_Status
		   N_Results:
		   N_FileID1&S_FileName1&S_Date&N_Size1
		   T_Description2
		   N_FileID2&S_FileName2&S_Date&N_Size2
		   T_Description2....

  Script Mapping:  FileCatalog
  DB Op:	   Select
*/

FileCatalog ( char *Arg[], int ArgCnt, char *MsgTxt) {
/*    int nproductid;*/
    char *ptr;
    int i;
    int len;
    int nprodid;
    int key;
    int num_of_fls;
    char Filename[FILENAMELEN+1];
    char Filedesc[FILEDESCLEN+1];

    ptr = MsgTxt;
    nprodid = atoi(Arg[0]);
    num_of_fls = 3 + (int)(gSConstants->Scaled_Load/8);
    key = gSConstants->Time%100;

    ptr +=  sprintf(ptr,OK_STATUS);
    for (i = 0; i < num_of_fls; i++) {
      len = 0;
      len += randomtext (Filename, FILENAMELEN-2, FILENAME);
      len += randomtext (Filedesc, FILEDESCLEN-2, FILEDESC);
      ptr += sprintf(ptr,"%06d&%s&2004-6-1 10:15am&%d&%s\n", 
	i + (nprodid*10), Filename, (key + i) * nprodid, Filedesc);
    }

    return 0;

}

/*
  Support Command: 8 - File 
  Request Params:  N_ProductID&S_DownloadCategory&S_Language&S_OS

  Response:	   S_Status
		   S_FileName&S_Date&N_Size&S_DownloadURL
		   T_Description
		   T_AdditionalInfo


  Script Mapping:  File
  DB Op:	   Select
*/

FileInfo ( char *Arg[], int ArgCnt, char *MsgTxt) {

/*    int nproductid;*/
    char *ptr;
    int len;
    int nprodid;
    int key;
    char Filename[FILENAMELEN+1];
    char Filedesc[FILEDESCLEN+1];
    char AdditionalInfo[ADDTNLINFOLEN+1];
    char Url[URLLEN+1];

    ptr = MsgTxt;
    nprodid = atoi(Arg[0]);
    key = gSConstants->Time%1000;

    ptr +=  sprintf(ptr,OK_STATUS);
    len = 0;
    len += randomtext (Filename, FILENAMELEN-2, FILENAME);
    len += randomtext (Filedesc, FILEDESCLEN-2, FILEDESC);
    len += randomtext (AdditionalInfo, ADDTNLINFOLEN-2, ADDTNLINFO);
    len += randomtext (Url, URLLEN-2, URL);
    ptr += sprintf(ptr,"%06d&%s&2004-6-1 10:15am&%d&%s\n%s\n%s\n",
      (nprodid*10), Filename, key * nprodid, Url, Filedesc, AdditionalInfo);


    return 0;
}

