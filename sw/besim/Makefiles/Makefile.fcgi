#CC	      = cc
#LD	      = cc
#LDFLAGS      =  -L/usr/local/lib
#LIBS         =  -lm -lfcgi
#DEFS         = -DUSE_MMAP
#OPTIMIZE     = +O
#CFLAGS	      = -I/usr/local/include -I./md5 
#DEST	      = .

EXTHDRS	      =

HDRS	      = besim_banking.h \
		besim_common.h \
		besim_ecommerce.h \
		besim_supportsite.h \
		randomtext.h \
		md5/global.h \
		md5/md5.h

INSTALL	      = install

MAKEFILE      = Makefile

OBJS	      = besim_banking.o \
		besim_ecommerce.o \
		besim_fcgi.o \
		besim_supportsite.o \
		randomtext.o

MD5_OBJS      = md5/md5c.o

PRINT	      = pr

PROGRAM       = besim_fcgi.fcgi

SRCS          = besim_banking.c \
		besim_ecommerce.c \
		besim_fcgi.c \
		besim_supportsite.c \
		randomtext.c \
	        md5/md5c.c

SYSHDRS	      =

all:		$(PROGRAM)

$(PROGRAM):     $(OBJS) $(MD5_OBJS) 
		@echo "Linking $(PROGRAM) ..."
		@echo '@$(LD) $(LDFLAGS) $(OBJS) $(MD5_OBJS) $(LIBS) -o $(PROGRAM)'
		@$(LD) $(LDFLAGS) $(OBJS) $(MD5_OBJS) $(LIBS) -o $(PROGRAM)
		@echo "done"

clean:;		@rm -f $(OBJS) core $(MD5_OBJS) 

clobber:;	@rm -f $(OBJS) $(PROGRAM) core tags $(MD5_OBJS)

depend:;	@mkmf -f $(MAKEFILE) ROOT=$(ROOT)

echo:;		@echo $(HDRS) $(SRCS)

index:;		@ctags -wx $(HDRS) $(SRCS)

install:	$(PROGRAM)
		@echo Installing $(PROGRAM) in $(DEST)
		@-strip $(PROGRAM)
		@if [ $(DEST) != . ]; then \
		(touch $(DEST)/$(PROGRAM); mv -f $(DEST)/$(PROGRAM) $(DEST)/$(PROGRAM)_`date +\%y\%m\%d\%H\%M\%S`; $(INSTALL) $(INSTALLARGS)); fi

print:;		@$(PRINT) $(HDRS) $(SRCS)

tags:           $(HDRS) $(SRCS); @ctags $(HDRS) $(SRCS)

update:		$(DEST)/$(PROGRAM)

$(MD5_OBJS):
	@echo '   $@'
	cd md5; make md5c.o CFLAGS="$(MD5_CFLAGS)"

#$(DEST)/$(PROGRAM): $(SRCS) $(HDRS) $(EXTHDRS)
#		@$(MAKE) -f $(MAKEFILE) ROOT=$(ROOT) DEST=$(DEST) install
