/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

#include "randomtext.h"
#include "besim_common.h"


int randomtext (char* textbuf, int textlength, int texttype) {
 
char *sentence;
char phrase[PHRASELEN];
int len = 0;
int rlen = 0;
int lnctr = 0;
int i,j,k,l,m,n, ii, jj, kk, ll, mm, nn;

     sentence = textbuf;

     i  = (rand())%5;
     j  = (rand())%10;
     k  = (rand())%20;
     l  = (rand())%20;
     m  = (rand())%25;
     n  = (rand())%30;
     ii  = (rand())%5;
     jj  = (rand())%10;
     kk  = (rand())%20;
     ll  = (rand())%20;
     mm  = (rand())%25;
     nn  = (rand())%30;

switch (texttype) {
case SHORTNAME:
	len = sprintf (phrase, "%s", Nouns[k]);
	if (textlength < len) phrase[textlength]='\0';
	strcpy (textbuf,phrase);
        break;

case LONGNAME:
	len = sprintf (phrase, "%s %s", Adjective[j], Nouns[k]);
	if (textlength < len) phrase[textlength]='\0';
	strcpy (textbuf,phrase);
        break;

case FEATURE:
	len = sprintf (phrase, "%s %s %s", 
		Superlative[j], Adjective[jj], Nouns[k]);
	if (textlength < len) phrase[textlength]='\0';
	strcpy (textbuf,phrase);
        break;

case COMPONENTID:
	len = sprintf (phrase, "%s-Model-%d", Nouns[k], k*100);
	if (textlength < len) phrase[textlength]='\0';
	strcpy (textbuf,phrase);
        break;

case TITLE:
	len = sprintf (phrase, "%s %s %s %s", 
		Article[i], Superlative[j], Adjective[jj], Nouns[l]);
	if (textlength < len) phrase[textlength]='\0';
	strcpy (textbuf,phrase);
        break;

case OVERVIEW:
	len = sprintf (phrase, "%s %s %s %s %s %s" , Article[ii], 
		adjective[j], adjective[jj], noun [l], verb[kk], adverb[j]);
	if (textlength < len) phrase[textlength]='\0';
	strcpy (textbuf,phrase);
        break;

case HIGHLIGHTS:
	len = sprintf (phrase, "%s %s %s", 
		Verb[kk], preposition[j], adjective[jj]);
	if (textlength < len) phrase[textlength]='\0';
	strcpy (textbuf,phrase);
        break;

case URL:
	len = sprintf (phrase, 
		"http://www.SPECweb2005-Ecommerce.web/%s/%s.script\?%s:%s", 
		Nouns[k],noun[k], Adjective[j], adjective[jj]);

	if (textlength < len) phrase[textlength]='\0';
	strcpy (textbuf,phrase);
        break;

case DESCRIPTION:
	
	while (rlen < textlength) {
	  if ((textlength - rlen) > 120) {
	    len = sprintf (sentence, " %s %s %s %s %s %s %s %s.",
		Article[i], adjective[j], noun [l], verb[k], 
		preposition[j], article[ii], adjective[jj], noun[ll]);
	  } else if ((textlength - rlen) > 80) {
	    len = sprintf (sentence, " %s %s %s %s %s!" ,
		 Article[i], adjective[jj], noun [l], verb[kk], adverb[j]);
	  } else if ((textlength - rlen) > 40) {
	    len = sprintf (sentence, " %s %s %s %s!" ,
		Article[i], noun [kk], verb[j], adverb[jj]);
	  } else if ((textlength - rlen) > 20) {
		len = sprintf (sentence, " Buy Now!");
	  } else {
		len = sprintf (sentence, "!");
	  }
	  rlen += len;
	  sentence += len;
          i  = (rand())%5;
          j  = (rand())%10;
          k  = (rand())%20;
          l  = (rand())%20;
          ii = (rand())%5;
          jj = (rand())%10;
          kk = (rand())%20;
          ll = (rand())%20;

	}	
	len = rlen;
	break;

case CURRENCIES:
	if (textlength > 0) {
	  len = sprintf (phrase, "%s", Currency[k]);
	  if (textlength < len) phrase[textlength]='\0';
	    strcpy (textbuf,phrase);
	}
	else {
	  k = (abs(textlength) %20);
	  
	  len = sprintf (phrase, "%s", Currency[k]);
	  strcpy (textbuf,phrase);
	}
        break;

case CATEGORY:
	len = sprintf (phrase, "%s %s", CatGroups[i], Nouns[m]);
	if (textlength < len) phrase[textlength]='\0';
	strcpy (textbuf,phrase);
        break;

case PRODUCT:
	len = sprintf (phrase, "%s %s%04d", 
		Nouns[mm], ModelType[ii], (rand())%1000);
	if (textlength < len) phrase[textlength]='\0';
	strcpy (textbuf,phrase);
        break;

case FILENAME:
	len = sprintf (phrase, "%s.%s", Software[ii], Suffix[i]);
        if (textlength < len) phrase[textlength]='\0';
        strcpy (textbuf,phrase);
        break;

case FILEDESC:
	len = sprintf (phrase, "This is the %s for the %s%04d %s %s using %s" , 
	   SuffixDesc[i], ModelType[i], i*10, CatGroups[i], Nouns[m], 
	   Software[ii]) ;
	if (textlength < len) phrase[textlength]='\0';
	strcpy (textbuf,phrase);
        break;

	
case ADDTNLINFO:

        textlength = textlength/2 + (textlength/20)*((rand())%10) + (textlength/20)*((rand())%2);
	while (rlen < textlength) {
	  if ((textlength - rlen) > 120) {
            lnctr++;
	    len = sprintf (sentence, "%d. %s %s %s %s %s %s %s %s %s.<BR>",
		lnctr, DirectVerb[l],article[i],adjective[j], noun[l], 
                directVerb[k], preposition[j], article[ii], adjective[jj], 
		noun[ll]);
	  } else if ((textlength - rlen) > 80) {
	    len = sprintf (sentence, "	%s %s %s %s %s." ,
		 Article[i], adjective[jj], noun[l], directVerb[kk], adverb[j]);
	  } else if ((textlength - rlen) > 40) {
	    len = sprintf (sentence, " %s %s %s %s." ,
		Article[i], noun [kk], verb[j], adverb[jj]);
	  } else if ((textlength - rlen) > 20) {
		len = sprintf (sentence, " Please Reboot Now.");
	  } else {
		len = sprintf (sentence, ".");
	  }
	  rlen += len;
	  sentence += len;
          i  = (rand())%5;
          j  = (rand())%10;
          k  = (rand())%20;
          l  = (rand())%20;
          ii = (rand())%5;
          jj = (rand())%10;
          kk = (rand())%20;
          ll = (rand())%20;

	}	
	len = rlen;
	break;


    }
    if (len > textlength) len = textlength;
    return (len);
}

