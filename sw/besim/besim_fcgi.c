/** Copyright (c) 2004-2005 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 */

/*

    SPECweb2005 Backend Simulator (BESIM)  FastCGI in C 

    Calling Sequence:

    GET /fast-cgi/specweb2005_besim?<Workload_Type>&<Command_Type>&\
	<param=value>[&<param=value>]


    Returns:

	HTTP 200 OK
        Cache-control: no-store
	Content-type: text/html
	Content-Length: [length of all return text - excluding headers]

	<html>
	<head><title>SPECweb2005 BESIM</title></head>
	<body>
	<p>SERVER_SOFTWARE = [ServerSoftware]
	<p>REMOTE_ADDR = [RemoteAddr]
	<p>SCRIPT_NAME = [ScriptName]
	<p>QUERY_STRING = [QueryString]
	<pre>
	[BeSIM MessageText]
	</pre>
	</body></html>

*/

#include "fcgi_stdio.h"
#include "besim_common.h"


void DoOutput(char *Buf, int Len);
void ProcessQuery (char *Buf, char *Qstring);
int ReadFile(char *FileName, char *Buf, int BufSize);
void DoBankingCommand(char *Arg[],int ArgCnt, char* MsgText);
void DoEcommerceCommand(char *Arg[],int ArgCnt, char* MsgText);
void DoSupportCommand(char *Arg[],int ArgCnt, char* MsgText);

int main(void) {
    int count = 0;
  int len;

  char *ServerSoftware;
  char *RemoteAddr;
  char *ScriptName;
  char *QueryString;
  char *TopDir;
  char *RequestMethod;

  char FileName[PATH_MAX];
  char Buffer[BUFLEN];
  char MsgText[BUFLEN];

  /* Process Requests in Fast-cgi Accept loop */

  while(FCGI_Accept() >= 0) {

    ServerSoftware = getenv("SERVER_SOFTWARE");
    RemoteAddr = getenv("REMOTE_ADDR");
    ScriptName = getenv("SCRIPT_NAME");
    QueryString = getenv("QUERY_STRING");
    RequestMethod = getenv("REQUEST_METHOD");

    /* Build Page Template from bolierplate and environment variables */

    TopDir = getenv ("DOCUMENT_ROOT");

    if (TopDir == NULL) {
      len = sizeof(DEFAULT_TOP_DIR);
      TopDir = malloc(len + 1);
      strncpy(TopDir, DEFAULT_TOP_DIR, len);
      TopDir[len] = '\0';
    }

    len = sprintf(Buffer, BOILERPLATE_START, ServerSoftware, RemoteAddr, 
		  ScriptName,QueryString);

    /* Process the query to obtain message text to complete the page */
  
    if (QueryString != NULL && strlen(QueryString) > 0) {
      ProcessQuery(MsgText, QueryString);
      strncpy(&Buffer[len], MsgText, strlen(MsgText));
      len += strlen(MsgText)-1;
    } else { /* no query string */
          sprintf(&Buffer[len], "%s%s", ERR1_STATUS, ERROR_NO_QUERY_STRING);
          len += sizeof(ERR1_STATUS)-1 ;
	  len += sizeof(ERROR_NO_QUERY_STRING)-2 ;
    }
  
    DoOutput(Buffer, len);
  }
  exit (0);
}

void ProcessQuery (char *Buf, char *Qstring) {
  int WorkloadType;
  int CommandType;
  char *pStr;
  char *Arg[100];
  int ArgCnt;

  /* Place each ampersand separated argument into the Arg list */

  Arg[0] = Qstring;
  for (ArgCnt = 0, pStr = Qstring; *pStr != '\0'; pStr++) {
    if ('&' == *pStr) {
      *pStr = '\0';
      ArgCnt++;
      Arg[ArgCnt] = pStr + 1;
    }
    else if ( ',' == *pStr) {
      *pStr = '\0';
      ArgCnt++;
      Arg[ArgCnt] = pStr + 1;
    }
  }

  WorkloadType = atoi(Arg[0]);
  CommandType = atoi(Arg[1]);

  /* Select and process workload command */

  switch (WorkloadType) {
case BANKING: 
    DoBankingCommand(Arg,ArgCnt,Buf);
    break;
case ECOMMERCE:
    DoEcommerceCommand(Arg,ArgCnt,Buf);
    break;
case SUPPORT:
    DoSupportCommand(Arg,ArgCnt,Buf);
    break;
default:
    sprintf(Buf, "%s%s", ERR1_STATUS, ERROR_UNDEF_WORKLOAD);
    break;
  }
  
} 

void DoOutput(char *Buf, int Len) {
 
  char Intro[128];
  int TempLen;

  /* Append last piece of HTML boilerplate to page */

  TempLen = strlen(BOILERPLATE_END) + 1;   
  memcpy(&Buf[Len], BOILERPLATE_END, TempLen);
  Len += TempLen - 1;
  
  /* Add HTTP headers to output */

  sprintf(Intro, 
   "Cache-control: no-store\r\nContent-type: text/html\r\nContent-Length: %d\r\n\r\n", 
   Len);

  /* Send completed response back through CGI/Fast-CGI interface */

  printf ("%s%s",Intro, Buf);
  

}

int 
ReadFile(char *FileName, char *Buf, int BufSize) {

  int Desc;
  int Len;

  if ( (Desc = open(FileName, O_RDONLY, NULL)) == -1)
    return sprintf(Buf, "Error opening file '%s': %s", FileName, 
		   strerror(errno));
  else {
    Len = read (Desc, (void *) Buf, BufSize);
    close(Desc);
    return Len;
  }
}

