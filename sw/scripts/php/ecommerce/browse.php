<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('inc.common.php');

    $frame->assign('title', 'Browse Page');

    // Check for required information
    checkGet('s');

    $body->assign('cust_type', $customer_types[$_GET['s']]);

    $products = backend_query(BACKEND_CMD_PRODUCT_LINE . '&'. $_GET['s']);
    $body->assign('products', $products);

    // Populate the frame template
    $frame->assign('body', $body->fetch('page.browse.htm'));
    if(is_file(PADDING_DIR . 'browse'))
        $frame->assign('padding',  file_get_contents(PADDING_DIR . 'browse'));
    else
        report('Unable to locate padding file.', REPORT_ERROR);
    renderPage();
?>
