<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('init_vars.php'); // server variables set by the prime client

    define('BACKEND_CMD_CATEGORIES',          1);
    define('BACKEND_CMD_PRODUCT_LISTING',     2);
    define('BACKEND_CMD_SEARCH',              3);
    define('BACKEND_CMD_DOWNLOAD_CATEGORIES', 4);
    define('BACKEND_CMD_LANGUAGES',           5);
    define('BACKEND_CMD_OPERATING_SYSTEMS',   6);
    define('BACKEND_CMD_FILE_LISTING',        7);
    define('BACKEND_CMD_FILE_DETAILS',        8);

    // Load in Smarty
    require_once(SMARTY_DIR.'Smarty.class.php');

	class SmartySupport extends Smarty {
		function SmartySupport(){
			$this->Smarty();
			$this->template_dir = SMARTY_SUPPORT_DIR . 'templates/';
			$this->compile_dir  = SMARTY_SUPPORT_DIR . 'templates_c/';
			$this->caching = false;
		}
	}

  /* Function takes a BeSim request and parse the results.
     Return value: an array of the data (if no error),
                   or an empty array if an error occurred */
    function backend_query($request) {

    $besim_host = BESIM_HOST;
    $besim_port = BESIM_PORT;

    if (BESIM_COUNT > 1) // multiple BeSim case
    {
       global $besim_hosts, $besim_ports;
       $random_besim_num = rand(0, BESIM_COUNT - 1);
       $besim_host = $besim_hosts[$random_besim_num];
       $besim_port = $besim_ports[$random_besim_num];
    }

	if (strstr($request, '&') !== false)
	        list($cmd, $params) = explode('&', $request, 2);
	else {
		$cmd = $request;
		$params = '';
	}

	if (!BESIM_PERSISTENT) {
           if (!$fp = fopen('http://'.$besim_host.':'.$besim_port.BESIM_URI."?3&$request", 'r')) {
              report('Could not open BeSim socket!');
              return array();
           }
           $response = '';
           while (!feof($fp)) {
              $response .= fgets($fp);
           }
           fclose($fp);
        } else { // BESIM_PERSISTENT == true
           if (!$fp = pfsockopen($besim_host, $besim_port))
           {
              report("Failed to open BeSim stream: $errstr ($errno)", REPORT_ERROR);
              return array();
           };
           fputs($fp, 'GET ' . BESIM_URI . "?3&$request HTTP/1.1\r\nHost: " . $besim_host . "\r\n\r\n");
           while ($line = fgets($fp)) { // read response line-by-line
              if (preg_match('/Content-Length: (.+)/i', $line, $matches)) {
	         $length = (int) trim($matches[1]); // length to fread
              } elseif ($line == "\r\n") { // end of HTTP headers
                 break;
              }
           }

           if (!isset($length) || $length == 0)
              error_page("Could not read content length from BeSim!");

           $response = '';
           for ($bytes_read = 0; $bytes_read < $length; $bytes_read += strlen($response)) {
                $response .= fread($fp, $length);
           }
        } // end persistent

        if (preg_match('#<pre>\n(\d)\n(.*)\n</pre>#s', $response, $matches)) {
             $errno = $matches[1];
             if($errno != 0)
                   error_page("BeSim returned with an error number: $errno");
             $body = explode("\n", $matches[2]);
             $r = array();
             foreach ($body as $line) {
               // states 4-6 don't have "&" in the lines returned
               if ($cmd >= 4 && $cmd <= 6)
                  $r[] = $line;
               else
                  $r[] = explode('&', $line);
             }
             return $r;
        } else {
             error_page('Invalid response from BeSim!');
        }
    }

function error_page($message)
{
    echo <<<EOT
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" >
<head>
	<meta http-equiv="content-type" content="text/html; charset=iso-8858-1" />
	<link href="style.css" rel="stylesheet" type="text/css">
	<title>SPECweb2005: Support Error</title>
</head>
<body>
<!-- SPECweb2005 Error Page -->
<div class="error">
	<h1>Script Error</h1>
	<p>$message</p></div>
</body>
</html>
EOT;
    exit;
}

// Checks to see if an ID is set; if not, redirects to the index page.
function checkGET($arg){
	if(!isset($_GET[$arg])){
        header("Location: http://{$_SERVER['HTTP_HOST']}".dirname($_SERVER['REQUEST_URI']).'/index.php');
		exit();
	}
}


  # Add appropriate response headers
  ob_start('send_response_headers');

  function send_response_headers($text){
    if (SEND_CONTENT_LENGTH) header('Content-Length: ' . strlen($text));
    header('Cache-Control: no-cache');
    return $text;
  }

//  session_start();

?>
