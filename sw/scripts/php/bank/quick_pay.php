<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

  require('inc.common.php');

  check_login();
  $userid=$_SESSION['userid'];
  if(empty($_POST['payee']) || !is_array($_POST['payee'])){
    show_msg('No payee selected');
    exit();
  }else{
    $payee=$_POST['payee'];
  }
  if(empty($_POST['date']) || !is_array($_POST['date'])){
    show_msg('No date selected');
    exit();
  }else{
    $date=$_POST['date'];
  }
  if(empty($_POST['amount']) || !is_array($_POST['amount'])){
    show_msg('No amount selected');
    exit();
  }else{
    $amount=$_POST['amount'];
  }
  foreach ($payee as $index => $id){
    // Ignore any line with empty payee_id field.
    if(empty($id)) continue;
    if(empty($date[$index])){
      $msg[]="Failed: payee={$id}  Reason: No date";
      continue;
    }
    $date_array=split_date($date[$index]);
    if(!$date_array){
      $msg[]="Failed: payee={$id}  Reason: Invalid date format";
      continue;
    }
    $today=localtime(time(), TRUE);
    if(compare_date($date_array[0], $date_array[1], $date_array[2],
                    $today['tm_year']+1900, $today['tm_mon']+1, $today['tm_mday'])<0){
      $msg[]="Failed: payee={$id}  Reason: Tried to schedule payment earlier than today";
      continue;
    }
    if(empty($amount[$index])){
      $msg[]="Failed: payee={$id}  Reason: No amount";
      continue;
    }
    $a=(float)$amount[$index];
    if($a<=0.0){
      $msg[]="Failed: payee={$id} Reason: Illegal amount";
      continue;
    }
    $request=BACKEND_CMD_QUICK_PAY.'&'.$userid.'&'.$id.'&'.$date[$index].'&'.$amount[$index];
    list($r, $errno) = backend_get_array($request);
    if($errno){
      $msg[]="Failed: payee={$id} Error Code: $errno";
      continue;
    }
    $msg[]="Scheduled: payee={$id}, date={$date[$index]}, amount=".$a;
  }
  $smarty=new SmartyBank;
  $smarty->assign("userid", $userid);
  $smarty->assign("msg", $msg);
  $smarty->assign('conf', $r);
  $smarty->display("quick_pay.tpl");
?>


