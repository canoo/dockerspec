<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>SPECweb2005: Order Checks</title>
  </head>
  <body bgcolor="white">
  <!-- SPECweb2005 Dynamic Data Area -->
  <table  summary="SPECweb2005_User_Id">
    <tr><th>User ID</th></tr>
    <tr><td>{$userid}</td></tr> 
  </table>
  <table summary="SPECweb2005_Acct_Balance" cellpadding=3 border=1>
    <tr>
      <th>Account</th>
      <th>Type</th>
      <th>Balance</th>
    </tr>
      {foreach item=acct from=$acct_balance}
        <tr><td>{$acct[0]}</td>
            <td>{if $acct[1] eq "1"}
                   Checking
                {elseif $acct[1] eq "2"}
                   Saving
                {else}
                   Other
                {/if}</td>
            <td>{$acct[2]}</td>
         </tr>
      {/foreach}
  </table>
  <!-- SPECweb2005 Displayable Page Title -->
  <table>
    <tr>
      <td><b><font color="#9400d3">SPECweb2005: Order Checks</font></b></td>
    </tr>
  </table>
  <!-- SPECweb2005 User Action Area -->
  {include file="menu.tpl"}
    <form method="post" action="place_check_order.php">
      <b>Please choose the check design below:</b><br>
      <table cellpadding="5" border=1>
      <tr>
        <td><input type="radio" name="check_design" value="1">
            <img src="images/check_01.jpg" align="middle" alt="check design #1"></td>
        <td><input type="radio" name="check_design" value="2">
            <img src="images/check_02.jpg" align="middle" alt="check design #2"></td>
      <tr>
        <td><input type="radio" name="check_design" value="3">
            <img src="images/check_03.jpg" align="middle" alt="check design #1"></td>
        <td><input type="radio" name="check_design" value="4">
            <img src="images/check_04.jpg" align="middle" alt="check design #4"></td>
      <tr>
        <td><input type="radio" name="check_design" value="5">
            <img src="images/check_05.jpg" align="middle" alt="check design #5"></td>
        <td><input type="radio" name="check_design" value="6">
            <img src="images/check_06.jpg" align="middle" alt="check design #6"></td>
      <tr>
        <td><input type="radio" name="check_design" value="7">
            <img src="images/check_07.jpg" align="middle" alt="check design #7"></td>
        <td><input type="radio" name="check_design" value="8">
            <img src="images/check_08.jpg" align="middle" alt="check design #8"></td>
      <tr>
        <td><input type="radio" name="check_design" value="9">
            <img src="images/check_09.jpg" align="middle" alt="check design #9"></td>
        <td><input type="radio" name="check_design" value="10">
            <img src="images/check_10.jpg" align="middle" alt="check design #10"></td>
      </table>
      <b>Number of checks: </b><input type="text" name="number"><br>
      <b>Charge account: </b><input type="text" name="account_no"><br>
      <input type="submit" value="Submit">
    </form>
  <!-- SPECweb2005 Image References -->
  <!-- SPECweb2005 Embedded Text -->
<pre>   
{include file=$PADDING_DIR|cat:"order_check"}
</pre>
  </body>
</html>
