<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>SPECweb2005: Welcome</title>
  </head>
  <body bgcolor="white">
  <!-- SPECweb2005 Dynamic Data Area -->
  <table  summary="SPECweb2005_User_Id">
    <tr><th>User ID</th></tr>
    <tr><td>{$userid}</td></tr> 
  </table>
  <table summary="SPECweb2005_Acct_Balance" cellpadding=3 border=1>
    <tr>
      <th>Account</th>
      <th>Type</th>
      <th>Balance</th>
    </tr>
      {foreach item=acct from=$acct_balance}
        <tr><td>{$acct[0]}</td>
            <td>{if $acct[1] eq "1"}
                   Checking
                {elseif $acct[1] eq "2"}
                   Saving
                {else}
                   Other
                {/if}</td>
            <td>{$acct[2]}</td>
         </tr>
      {/foreach}
  </table>
  <!-- SPECweb2005 Displayable Page Title -->
  <table>
    <tr>
      <td><b><font color="#9400d3">SPECweb2005: Welcome User</font></b></td>
    </tr>
  </table>
  <!-- SPECweb2005 User Action Area -->
  {include file="menu.tpl"}
  <!-- SPECweb2005 Image References -->
  <!-- SPECweb2005 Embedded Text -->
<pre>   
{include file=$PADDING_DIR|cat:"welcome"}
</pre>
  </body>
</html>
