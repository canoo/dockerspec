<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>SPECweb2005: Transfer Result</title>
  </head>
  <body bgcolor="white">
  <!-- SPECweb2005 Dynamic Data Area -->
  <table  summary="SPECweb2005_User_Id">
    <tr><th>User ID</th></tr>
    <tr><td>{$userid}</td></tr> 
  </table>
  <table summary="SPECweb2005_Transfer_Summary" cellpadding=3 border=1>
    <tr>
      <th>From</th>
      <th>To</th>
      <th>Amount</th>
    </tr>
    <tr>
      <td>{$from}</td>
      <td>{$to}</td>
      <td>{$amount}</td>
    </tr>
  </table>
  <table summary="SPECweb2005_Acct_Balance" cellpadding=3 border=1>
    <tr>
      <th>Account</th>
      <th>New Balance</th>
    </tr>
      {foreach item=acct from=$acct_balance}
        <tr>
          <td>{$acct[0]}</td>
          <td>{$acct[1]}</td>
        </tr>
      {/foreach}
  </table>
  <!-- SPECweb2005 Displayable Page Title -->
  <table>
    <tr>
      <td><b><font color="#9400d3">SPECweb2005: Transfer Result</font></b></td>
    </tr>
  </table>
  <!-- SPECweb2005 User Action Area -->
  {include file="menu.tpl"}
  <!-- SPECweb2005 Image References -->
  <!-- SPECweb2005 Embedded Text -->
<pre>   
{include file=$PADDING_DIR|cat:"post_transfer"}
</pre>
  </body>
</html>
