<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */
    require('inc.common.php');

    // Check if session exists
    if (empty($_SESSION))
        error_page('Session does not exist');

    $frame->assign('title', 'Billing Details');

    // Check for required information
    checkGet('c', 's');
    if(empty($_SESSION['email']))
        report("User is not logged in", REPORT_ERROR);

    $body->assign('cust_type', $customer_types[$_GET['s']]);

    // Validate form data
    if (isset($_POST['bill_submit'])) {
        $reqFields = array('fname', 'lname', 'address', 'city', 'cc_type', 'state', 'zip', 'cc_num', 'phone', 'cc_expmonth', 'cc_expyear');
        $error = false;
        foreach($reqFields as $field)
            if(empty($_POST[$field])) {
                report("`$field` is a required field.", REPORT_WARNING);
                $error = true;
            }

        if(!preg_match("/[a-zA-Z]{2}/", $_POST['state'])) {
            report("State field invalid.", REPORT_WARNING);
            $error = true;
        }

        if(!preg_match("/(\d{5}-\d{4}|\d{5})/", $_POST['zip'])) {
            report("Zip field invalid.", REPORT_WARNING);
            $error = true;
        }

        if(!preg_match("/\d{16}/", $_POST['cc_num'])) {
            report("Credit card field is invalid.", REPORT_WARNING);
            $error = true;
        }

        if(!preg_match("/\d{3}-\d{3}-\d{4}/", $_POST['phone'])) {
            report("Phone field is invalid.", REPORT_WARNING);
            $error = true;
        }

        if(!$error){
            foreach ($_POST as $key => $value)
                if ($key != "bill_submit")
                    $_SESSION['bill'][$key] = $value;

        // if successful, redirect to confirm
        redirect("/confirm.php?{$_SERVER['QUERY_STRING']}", true);
        }
    }

    // Populate the frame template
    $frame->assign('body', $body->fetch('page.billing.htm'));
    if(is_file(PADDING_DIR . 'billing'))
        $frame->assign('padding',  file_get_contents(PADDING_DIR . 'billing'));
    else
        report('Unable to locate padding file.', REPORT_ERROR);
    renderPage();
?>
