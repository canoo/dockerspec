<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('inc.common.php');

    // Check if session exists
    if (empty($_SESSION))
        error_page('Session does not exist');

    $frame->assign('title', 'Shipping Details');

    // Check for required information
    checkGet('c', 's');
    if(empty($_SESSION['email']))
        report("User is not logged in", REPORT_ERROR);

    $body->assign('cust_type', $customer_types[$_GET['s']]);

    // Validate form data
    if (isset($_POST['ship_submit'])) {
        $reqFields = array('fname', 'lname', 'address', 'city', 'shipping', 'state', 'zip', 'shipping');
        $error = false;
        foreach($reqFields as $field)
            if(empty($_POST[$field])) {
                report("`$field` is a required field.", REPORT_WARNING);
                $error = true;
            }

        if(!preg_match("/[a-zA-Z]{2}/", $_POST['state'])) {
            report("State field is invalid.", REPORT_WARNING);
            $error = true;
        }
        if(!preg_match("/(\d{5}-\d{4}|\d{5})/", $_POST['zip'])) {
            report("Zip field is invalid.", REPORT_WARNING);
            $error = true;
        }
        if(!preg_match("/\d{3}-\d{3}-\d{4}/", $_POST['phone'])) {
            report("Phone field is invalid.", REPORT_WARNING);
            $error = true;
        }

        if(!$error){
        // store all the shipping information in the session
            foreach ($_POST as $key => $value)
                if ($key != "ship_submit")
                    $_SESSION['ship'][$key] = $value;
            redirect("/billing.php?{$_SERVER['QUERY_STRING']}", true);
        }
   }

    // Populate the frame template
    $frame->assign('body', $body->fetch('page.shipping.htm'));
    if(is_file(PADDING_DIR . 'shipping'))
        $frame->assign('padding',  file_get_contents(PADDING_DIR . 'shipping'));
    else
        report('Unable to locate padding file.', REPORT_ERROR);
    renderPage();
?>
