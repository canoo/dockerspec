<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('inc.common.php');

    $frame->assign('title', 'Customize Product');

    // Check for required information
    checkGet('s', 'c', 'i');

    $body->assign('cust_type', $customer_types[$_GET['s']]);

    // Check and update session variables
    if (empty($_SESSION['page'])) {
        $_SESSION['page']          = 1;
        $_SESSION['backend_price'] = 0;
    }
    if (isset($_GET['page']))
        $_SESSION['page']=$_GET['page'];

    if (isset($_POST['customize_submit'])){
        foreach($_POST as $component_type => $selection)
            if ($component_type != 'customize_submit') $_SESSION[$_GET['i']][$component_type] = $selection;
        $request = BACKEND_CMD_GETPRICE . '&' . urlencode($_GET['i']);
        foreach($_SESSION[$_GET['i']] as $selection) $request .= "&$selection";
        $backend_price = backend_query($request);

        $_SESSION['backend_price'] = $backend_price[0];
        $_SESSION['backend_currency'] = $backend_price[1];

        if ($_POST['customize_submit'] == "Continue"){
            if ($_SESSION['page'] < 3) $_SESSION['page']++;
        } elseif ($_POST['customize_submit'] == "AddtoCart") {
            unset($_SESSION['page']);
            redirect("/cart.php?{$_SERVER['QUERY_STRING']}");
        }
    }
    // Format product information and calculate price
    $configchoices = backend_query(BACKEND_CMD_CUSTOMIZE . "&$_GET[s]&" . urlencode($_GET['i']) . "&$_SESSION[page]");
    foreach ($configchoices as $key => $value) {
       $configchoices[$key] = explode('&', $value);
    }

    $item = array();
    $total_price = $_SESSION['backend_price'];
    foreach($configchoices as $choice){
        if(empty($_POST[$choice[0]])) $_POST[$choice[0]] = $choice[2];
        if($_POST[$choice[0]] == $choice[2]) $total_price += $choice[3];
        $item[$choice[0]][] = array(
            'name'     => $choice[1],
            'id'       => $choice[2],
            'price'    => $choice[3],
            'currency' => $choice[4],
            'selected' => ($_POST[$choice[0]] == $choice[2]) //?
        );
    }

    // Populate the body template
    $body->assign('configchoices', $item);
    $body->assign('total_price', number_format($total_price, 2));
    $body->assign('currency', (isset($_SESSION['backend_currency']) ? $_SESSION['backend_currency'] : 'USD'));

    // Populate the frame template
    $frame->assign('body', $body->fetch('page.customize.htm'));

    if(is_file(PADDING_DIR . 'customize'))
        $frame->assign('padding',  file_get_contents(PADDING_DIR . 'customize'));
    else
        report('Unable to locate padding file.', REPORT_ERROR);

    renderPage();
?>
