<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('inc.common.php');

    if(empty($_GET['c'])) $_GET['c'] = 'CAD01';

    $frame->assign('title', 'Home Page');

    // Populate the body template
    $regions = backend_query(BACKEND_CMD_GET_REGIONS);
    $body->assign('regions', $regions);
    $body->assign('customer_types', $customer_types);

    // Populate the frame template
    $frame->assign('body', $body->fetch('page.home.htm'));
    if(is_file(PADDING_DIR . 'index'))
        $frame->assign('padding',  file_get_contents(PADDING_DIR . 'index'));
    else
        report('Unable to locate padding file.', REPORT_ERROR);

    renderPage();
?>
