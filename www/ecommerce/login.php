<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

    require('inc.common.php');

    // Check if session exists
    if (empty($_SESSION))
        error_page('Session does not exist');

    $frame->assign('title', 'Login Page');

    // Check for required information
    checkGet('c', 's');

    $body->assign('cust_type', $customer_types[$_GET['s']]);

    // Handle login requests
    if (isset($_POST['login_submit'])) {
        if (empty($_POST['email']) || empty($_POST['password'])) {
           report('Please fill in values for email and password', REPORT_WARNING);
        } else {
           // to keep things simple, email == password
           $passwd = md5(rawurlencode($_POST['password']));
           $real_passwd = backend_query(BACKEND_CMD_LOGIN.'&'.rawurlencode($_POST['email']));
           if ($passwd == $real_passwd[0])
               $_SESSION['email'] = $_POST['email'];
           else
              report('Incorrect email/password pair', REPORT_WARNING);
        }
    } elseif (isset($_POST['register_submit'])) {
        $reqFields = array('email', 'email2', 'password', 'password2', 'fname', 'lname');
        $error = false;
        foreach($reqFields as $field)
            if(empty($_POST[$field])) {
                report("`$field` is a required field.", REPORT_WARNING);
                $error = true;
            }

        if (!preg_match("/^([a-zA-Z0-9_\-\.]+)@((([0-9]{1,3}\.){3}[0-9]{1,3})|([a-zA-Z]+\.)+[a-zA-Z]{2,4})/", $_POST['email'])) {
            report('The email field is invalid.', REPORT_WARNING);
            $error = true;
        } elseif ($_POST['email'] != $_POST['email2']) {
            report('The email field do not match.', REPORT_WARNING);
            $error = true;
        }
        if ($_POST['password'] != $_POST['password2']){
            report('The passwords fields do not match.', REPORT_WARNING);
            $error = true;
        }

        if(!$error){
            $confirm = backend_query(BACKEND_CMD_REGISTER."&$_POST[fname]&$_POST[lname]&".rawurlencode($_POST['email'])."&$_POST[password]");
            $_SESSION['user_registration_confirmation'] = $confirm[0];
            $_SESSION['email'] = $_POST['email'];
        }
    }

    if (isset($_SESSION['email']))
        if (isset($_GET['action'])) {
            if ($_GET['action'] == "SaveCart")
                redirect("/cart.php?{$_SERVER['QUERY_STRING']}");
            else
                redirect("/shipping.php?{$_SERVER['QUERY_STRING']}", true);
        }

    // Populate the frame template
    $frame->assign('body', $body->fetch('page.login.htm'));

    if(is_file(PADDING_DIR . 'login'))
        $frame->assign('padding',  file_get_contents(PADDING_DIR . 'login'));
    else
        report('Unable to locate padding file.', REPORT_ERROR);

    renderPage();
?>
