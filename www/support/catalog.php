<?PHP

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

	// Include required objects and functions
	require_once('inc.common.php');

	// Redirect to home page if required information is not present
    checkGET('id');

	// Initalize template engines
	$frameTemplate = new SmartySupport;
	$bodyTemplate  = new SmartySupport;

	// Connect to the datasource
    $product_listing = backend_query(BACKEND_CMD_PRODUCT_LISTING . '&' . $_GET['id']);

	// Populate Templates
	$bodyTemplate->assign('productListing', $product_listing);

	$frameTemplate->assign('title', "Category $_GET[id]");
	$frameTemplate->assign('body', $bodyTemplate->fetch('page.catalog.htm'));

    if(is_file(PADDING_DIR . 'catalog'))
        $frameTemplate->assign('padding',  file_get_contents(PADDING_DIR . 'catalog'));
    else
        echo('Unable to locate padding file.');

	// Render the page to the browser
	$frameTemplate->display('tpl.main.htm');
?>
