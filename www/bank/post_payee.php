<?php

/* Copyright (c) 2004-2006 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */
  require('inc.common.php');

  function backend_add_payee($userid, $payee_info){
    $request=BACKEND_CMD_POST_PAYEE.'&'.$userid.'&'.
             rawurlencode($payee_info['payee_id']).'&'.
             rawurlencode($payee_info['name']).'&'.
             rawurlencode($payee_info['street']).'&'.
             rawurlencode($payee_info['city']).'&'.
             rawurlencode($payee_info['state']).'&'.
             rawurlencode($payee_info['zip']).'&'.
             rawurlencode($payee_info['phone']);
    $r=backend_get_array($request);
    return $r;
  }

  check_login();
  $userid=$_SESSION['userid'];
  if(empty($_POST['payee_id']) ||
     empty($_POST['name']) ||
     empty($_POST['street']) ||
     empty($_POST['city']) ||
     empty($_POST['zip']) ||
     empty($_POST['state']) ||
     empty($_POST['phone'])){
    show_msg('No fields should be left blank');
    exit();
  }
  $payee_info=array('payee_id'=>$_POST['payee_id'],
                     'name'=>$_POST['name'],
                     'street'=>$_POST['street'],
                     'city'=>$_POST['city'],
                     'zip'=>$_POST['zip'],
                     'state'=>$_POST['state'],
                     'phone'=>$_POST['phone']);
  list($status, $errno)=backend_add_payee($userid, $payee_info);
  if($errno!=0){
    show_msg("Error code: $errno");
    exit();
  }
  $smarty=new SmartyBank;
  $smarty->assign('userid', $userid);
  $smarty->assign('msg', "Payee ".$_POST['payee_id']." succesfully added");
  $smarty->assign('conf', $status);
  $smarty->display('post_payee.tpl');
?>
