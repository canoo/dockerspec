<?php
  require('inc.common.php');
  
  function backend_get_account_summary($userid){
    $request=BACKEND_CMD_ACCT_SUMMARY.'&'.$userid;
    $a=backend_get_array($request);
    return $a;
  }
  
  check_login();
  $smarty=new SmartyBank;
  list($summary, $errno)=backend_get_account_summary($_SESSION['userid']);
  if($errno==0){
    $smarty->assign('userid', $_SESSION['userid']);
    $smarty->assign('summary', $summary);
    $smarty->display('account_summary.tpl');
  }else{
    show_msg("Error code: $errno");
  }
?>
  
