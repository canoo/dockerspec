<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>SPECweb2005: Check Detail Output</title>
  </head>
  <body bgcolor="white">
  <!-- SPECweb2005 Dynamic Data Area -->
  <table  summary="SPECweb2005_User_Id">
    <tr><th>User ID</th></tr>
    <tr><td>{$userid}</td></tr> 
  </table>
  <table summary="SPECweb2005_Check_Details" align="center" cellpadding=3 border=1>
    <tr>
      <th>Account Number:</th>
      <td>{$account_no}</td>
    </tr>
    <tr>
      <th>Check Number:</th>
      <td>{$check_no}</td>
    </tr>
    <tr>
      <th>Front Image:</th>
      <td><img alt="front image" src="check_detail_image.php?side=front&check_no={$check_no}"></td>
    </tr>
    <tr>
      <th>Back Image:</th>
      <td><img alt="back image" src="check_detail_image.php?side=back&check_no={$check_no}"></td>
    </tr>
  </table>
  <!-- SPECweb2005 Displayable Page Title -->
  <table>
    <tr>
      <td><b><font color="#9400d3">SPECweb2005: Check Details Output</font></b></td>
    </tr>
  </table>
  <!-- SPECweb2005 User Action Area -->
  {include file="menu.tpl"}
  <!-- SPECweb2005 Image References -->
  <!-- SPECweb2005 Embedded Text -->
<pre>   
{include file=$PADDING_DIR|cat:"check_detail_html"}
</pre>
  </body>
</html>
      
