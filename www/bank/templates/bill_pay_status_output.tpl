<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" 
"http://www.w3.org/TR/html4/loose.dtd">
<html>
  <head>
    <title>SPECweb2005: Bill Pay Status</title>
  </head>
  <body bgcolor="white">
  <!-- SPECweb2005 Dynamic Data Area -->
  <table  summary="SPECweb2005_User_Id">
    <tr><th>User ID</th></tr>
    <tr><td>{$userid}</td></tr> 
  </table>
    <table summary="SPECweb2005_Bill_Pay_Status_Result" cellpadding=3 border=1 >
    <tr>
      <th>Payee</th>
      <th>Date</th>
      <th>Amount</th>
      <th>Status</th>
    {foreach item=row from=$status}
      <tr>
      <td>{$row[0]}</td>
      <td>{$row[1]}</td>
      <td align="right">{$row[2]}</td>
      <td>Pending</td>
      </tr>
    {/foreach}
    </tr>
    </table>
  <!-- SPECweb2005 Displayable Page Title -->
  <table>
    <tr>
      <td><b><font color="#9400d3">SPECweb2005: Bill Pay Status</font></b></td>
    </tr>
  </table>
  <!-- SPECweb2005 User Action Area -->
  {include file="menu.tpl"}
  <!-- SPECweb2005 Image References -->
  <!-- SPECweb2005 Embedded Text -->
<pre>   
{include file=$PADDING_DIR|cat:"bill_pay_status_output"}
</pre>
  </body>
</html>
    
